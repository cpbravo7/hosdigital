from crum import get_current_user
from django.db import models
from datetime import datetime

from core.modelsauditoria import BaseModel


class Especialidad(BaseModel):
    nombre = models.CharField(max_length=50, unique=True)
    descripcion = models.CharField(max_length=300, null=True, blank=True)
    # fecha_registro = models.DateTimeField(auto_now_add=True, null=True, blank=True, verbose_name='Fecha de Registro')
    # fecha_actualizacion = models.DateTimeField(auto_now=True, verbose_name='Fecha de Actualización')
    # persona_registro = models.CharField(max_length=50, verbose_name='Usuario de Registro', null=True, blank=True)
    # persona_actualizacion = models.CharField(max_length=50, verbose_name='Usuario de Actualización', null=True,
    #                                          blank=True)
    estado_especialidad = models.BooleanField(default=True, verbose_name='Activo', null=True, blank=True)

    def __str__(self):
        return self.nombre

    #    return 'Nro: {} / Nombre: {}'.format(self.id, self.nombre)
    # transfomar numero a string: return str(self.id)

    class Meta:
        verbose_name = 'Especialidad'
        verbose_name_plural = 'Especialidades'
        db_table = 'Especialidad'
        ordering = ['id']
        # No indicar campos hacia la pantalla EXCLUDE REVISAR

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        user = get_current_user()
        if user is not None:
            if not self.pk:
                self.persona_registro = user
            else:
                self.persona_actualizacion = user
        super(Especialidad, self).save()


class Persona(models.Model):
    nombres = models.CharField(max_length=255, verbose_name='Nombres')
    apellidos = models.CharField(max_length=255, verbose_name='Apellidos')
    identificacion = models.PositiveIntegerField(unique=True, verbose_name='Dni')
    telefono = models.PositiveIntegerField(unique=True, verbose_name='Teléfono')
    correo_electronico = models.Field
    direccion = models.Field
    ciudad_nacimiento = models.CharField(max_length=255)
    ciudad_residencia = models.CharField(max_length=255)
    fecha_nacimiento = models.DateField(default=datetime.now, verbose_name='Fecha de nacimiento')
    genero = models.CharField(max_length=15)
    usuario_hosdigital = models.CharField(max_length=50, verbose_name='Usuario Hosdigital')
    # Relacion muchos a muchos
    especialidad = models.ManyToManyField(Especialidad, blank=True)

    # Relacion uno a muchos
    # especialidad = models.ForeignKey(Especialidad, on_delete=models.PROTECT)

    def __str__(self):
        return 'Nombre: {} / Apellido: {}'.format(self.nombres, self.apellidos)

    #     return self.nombres

    class Meta:
        verbose_name = 'Persona'
        verbose_name_plural = 'Personas'
        db_table = 'Persona'
        ordering = ['id']


class Cita(models.Model):

    Consulta = 'Consulta Médica'
    Revisionexamenes = 'Revisión de Exámenes'

    motivo_choices = [
        (Consulta, 'Consulta Médica'),
        (Revisionexamenes, 'Revisión de Exámenes'),
    ]

    motivo = models.CharField(max_length=25, verbose_name='Motivo de Cita', choices=motivo_choices)
    persona = models.ForeignKey(Persona, on_delete=models.CASCADE, null=True, blank=True)
    especialidad = models.ForeignKey(Especialidad, on_delete=models.CASCADE, null=True, blank=True)
    disponibilidad = models.DateField(default=datetime.now, verbose_name='Disponibilidad')

    Atendido = '1'
    Agendado = '0'
    Cancelado = '-'

    estado_choices = [
        (Atendido, 'Cita Atendida'),
        (Agendado, 'Cita Agendada'),
        (Cancelado, 'Cita Cancelada'),
    ]

    estado_cita = models.CharField(max_length=2, default=True, verbose_name='Estado de Cita', choices=estado_choices)

    def __str__(self):
        return self.especialidad.nombre

    class Meta:
        verbose_name = 'Cita'
        verbose_name_plural = 'Citas'
        ordering = ['id']
