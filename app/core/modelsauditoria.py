from django.db import models
from django.conf import settings


# Modelo de auditoria
class BaseModel(models.Model):
    persona_registro = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                         related_name='user_registro', null=True, blank=True)
    persona_actualizacion = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                              related_name='user_actualizacion', null=True, blank=True)
    fecha_registro = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True, null=True, blank=True)

    class Meta:
        abstract = True
