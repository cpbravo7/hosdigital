from django.shortcuts import redirect
from datetime import datetime

from django.urls import reverse_lazy
from django.http import HttpResponseRedirect


class IsSuperuserMixin(object):

    # Redireccion hacia el template correspondiente en caso sea un Super user
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_superuser:
            return super().dispatch(request, *args, **kwargs)
        return redirect('index')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['date_now'] = datetime.now()
        return context


# Redireccion hacia el template correspondiente de acuerdo al Permiso
# que no aparezca el codigo 403 / REVISAR NO DIRECCIONA
class ValidatePermissionRequiredMixin(object):
    permission_required = ''
    url_redirect = None

    def get_perms(self):
        if isinstance(self.permission_required, str):
            perms = (self.permission_required,)
        else:
            perms = self.permission_required
        return perms

    def get_url_redirect(self):
        if self.url_redirect is None:
            return reverse_lazy('login')
        return self.url_redirect

    def dispatch(self, request, *args, **kwargs):
        if request.user.has_perms(self.get_perms()):
            return super().dispatch(request, *args, **kwargs)
        return redirect(self.get_url_redirect())
