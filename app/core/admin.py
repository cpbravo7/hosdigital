from django.contrib import admin
from core.models import *

# Register your models here.
admin.site.register(Especialidad)
admin.site.register(Persona)
admin.site.register(Cita)
