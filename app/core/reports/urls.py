from django.urls import path
from core.reports.views import ReportCitaView


urlpatterns = [
    path('cita/', ReportCitaView.as_view(), name='cita_reports'),
 ]