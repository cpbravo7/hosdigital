from django.forms import *


# Form especifica cada campo creado en un formulario
class ReportForm(Form):
    date_range = CharField(widget=TextInput(attrs={
        'class': 'form-control',
        'autocomplete': 'off',
    }))
