from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from core.forms import CitaForm
from core.mixins import IsSuperuserMixin, ValidatePermissionRequiredMixin
from core.models import *


# def especialidad_list(request):
#     data = {
#         'title': 'Listado de Especialidades',
#         'especialidades': Especialidad.objects.all()
#     }
#     return render(request, 'especialidad/list.html', data)
#

class citaListView(LoginRequiredMixin, ValidatePermissionRequiredMixin, ListView):
    # Especifica que permisos son requeridos para ingresar al listview despues de ser asignados por el administrador
    # en caso no tenga permiso se dirige a la pagina 404 Forbidden o a la clase creada.
    # permission_required = ('')
    model = Cita
    template_name = 'cita/list.html'

    # Para sobreescribir el comportamiento de ListView
    # def get_queryset(self):
    #    return Especialidad.objects.filter(nombre__istartswith='G')
    @method_decorator(csrf_exempt)
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Listado de Citas'
        context['entity'] = 'Citas'
        context['create_url'] = reverse_lazy('hosdigital:cita_createview')
        context['list_url'] = reverse_lazy('hosdigital:cita_list')
        # Alterar el comportamiento de List_View modificando los modelos object_list
        # context['objet_list'] = Persona.objects.all()
        return context


class citaCreateView(CreateView):
    model = Cita
    form_class = CitaForm
    template_name = 'cita/create.html'
    success_url = reverse_lazy('hosdigital:cita_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Crear una nueva cita'
        context['entity'] = 'Citas'
        context['list_url'] = reverse_lazy('hosdigital:cita_list')
        context['action'] = 'add'
        return context


class citaUpdateView(UpdateView):
    model = Cita
    form_class = CitaForm
    template_name = 'cita/create.html'
    success_url = reverse_lazy('hosdigital:cita_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Edición de Citas'
        context['entity'] = 'Citas'
        context['list_url'] = reverse_lazy('hosdigital:cita_list')
        context['action'] = 'edit'
        return context


class citaDeleteView(DeleteView):
    model = Cita
    template_name = 'cita/delete.html'
    success_url = reverse_lazy('hosdigital:cita_list')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Eliminación de citas'
        context['entity'] = 'Citas'
        context['list_url'] = reverse_lazy('hosdigital:cita_list')
        return context
