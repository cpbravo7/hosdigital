from django.urls import path

from core.views.agendamiento.cita import AgendamientoCreateView
from core.views.cita.views import *
from core.views.especialidad.views import *

# Nombre del bloque de rutas secundarias

app_name = 'hosdigital'

urlpatterns = [
    # Especialidad
    path('especialidad/list', especialidadListView.as_view(), name='especialidad_list'),
    path('especialidad/create', especialidadCreateView.as_view(), name='especialidad_createview'),
    path('especialidad/edit/<int:pk>/', especialidadUpdateView.as_view(), name='especialidad_updateview'),
    path('especialidad/delete/<int:pk>/', especialidadDeleteView.as_view(), name='especialidad_delete'),
    # Cita
    path('cita/list', citaListView.as_view(), name='cita_list'),
    path('cita/create', citaCreateView.as_view(), name='cita_createview'),
    path('cita/edit/<int:pk>/', citaUpdateView.as_view(), name='cita_updateview'),
    path('cita/delete/<int:pk>/', citaDeleteView.as_view(), name='cita_delete'),
    # Agendamiento
    path('agendamiento/cita', AgendamientoCreateView.as_view(), name='agendamiento_cita'),
]
