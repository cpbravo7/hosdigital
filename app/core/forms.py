from datetime import datetime
from django.forms import *

from core.models import Especialidad, Cita


class EspecialidadForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'
            self.fields['nombre'].widget.attrs['autofocus'] = True

    class Meta:
        model = Especialidad
        fields = '__all__'
        #  Widgets me permite personalizar mis componentes,
        #  campos de relleno del formulario.
        widgets = {
            'nombre': TextInput(
                # Attrs permite aumentar atributos
                # a los campos
                attrs={
                    # 'class': 'form-control', Propiedades de los campos
                    # de relleno extensivos
                    'placeholder': 'Ingrese un nombre',

                },
            ),
            'descripcion': Textarea(
                attrs={
                    'placeholder': 'Ingrese la descripción',
                    'rows': '3',
                    'columns': '3'
                }
            )

        }


class CitaForm(ModelForm):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for form in self.visible_fields():
            form.field.widget.attrs['class'] = 'form-control'
            form.field.widget.attrs['autocomplete'] = 'off'

            # Para ser solamente de lectura un campo
            # 'subtotal': TextInput(attrs={
            # 'readonly': True,

    class Meta:
        model = Cita
        fields = '__all__'
        #  Widgets me permite personalizar mis componentes,
        #  campos de relleno del formulario.
        widgets = {
            'motivo': Select(
                # Attrs permite aumentar atributos
                # a los campos
                attrs={
                    # 'class': 'form-control', Propiedades de los campos
                    # de relleno extensivos
                    'class': 'form-control select2',
                    'style': 'width: 100%',
                },
            ),
            'persona': Select(
                attrs={
                    'class': 'form-control select2',
                    'style': 'width: 100%',
                }
            ),
            'especialidad': Select(
                attrs={
                    'class': 'form-control select2',
                    'style': 'width: 100%',
                }
            ),
            'disponibilidad': DateInput(format='%Y-%m-%d',
                                        attrs={
                                            'value': datetime.now().strftime('%Y-%m-%d'),
                                            'autocomplete': 'off',
                                            'class': 'form-control datetimepicker-input',
                                            'id': 'disponibilidad',
                                            'data-target': '#disponibilidad',
                                            'data-toggle': 'datetimepicker'
                                        }
                                        ),
            'estado_cita': Select(
                attrs={
                    'class': 'form-control select2',
                    'style': 'width: 100%',
                }
            )
        }


