
from app.wsgi import *
# from core.models import Especialidad

# Listar
#query = Especialidad.objects.all()
#print(query)

# Ingresar
#t = Especialidad(descripcion='obstetriz').save()

# Editar
#e = Especialidad.objects.get(id=1)
#e.descripcion = 'Veterinario'
#e.fecha_registro = '2022-12-13'
#e.fecha_modificacion = '2022-12-13'
#e.fecha_creacion = '2022-12-13'
#e.save()

# Eliminar
#e = Especialidad.objects.get(id=1)
#e.delete()
#e.save()

# Listar Filtro
#y = Especialidad.objects.filter(descripcion__istartswith='o')
#print(y)

#y = Especialidad.objects.filter(descripcion__contains='den')
#print(y)
#
# y = Especialidad.objects.filter(descripcion__contains='obs').count()
# print(y)

#y = Especialidad.objects.filter(descripcion__contains='obs').exclude(id='4')
#print(y)

#for y in Especialidad.objects.filter(descripcion__contains='obs'):
#    print(y)

#for y in Especialidad.objects.filter(descripcion__contains='obs'):
#    print(y)

#for y in Especialidad.objects.filter(descripcion__contains='obs')[:1]:
#    print(y)

#za = Medico.objects.filter(especialidad_id='1')
#print(za)


