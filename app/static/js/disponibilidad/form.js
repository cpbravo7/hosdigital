var disponibilidad = {
    items: {
        motivo: '',
        citas: []
    }
}


$(function () {
    $('#disponibilidad').datetimepicker({
        format: 'YYYY-MM-DD',
        date: moment().format('YYYY-MM-DD'),
        locale: 'es',
        minDate: moment().format('YYYY-MM-DD')
    });
});